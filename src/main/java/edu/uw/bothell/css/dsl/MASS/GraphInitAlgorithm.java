package edu.uw.bothell.css.dsl.MASS;

public enum GraphInitAlgorithm {
    FULL_LIST,
    PARTITIONED_LIST,
}
