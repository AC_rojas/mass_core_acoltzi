package edu.uw.bothell.css.dsl.MASS.Parallel_IO;

/**
 * Created by Michael on 4/17/17.
 */
@SuppressWarnings("serial")
public class UnsupportedBufferTypeException extends Exception {
    public UnsupportedBufferTypeException(String message) {
        super(message);
    }
}
