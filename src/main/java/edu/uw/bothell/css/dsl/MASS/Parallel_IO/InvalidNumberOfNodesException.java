package edu.uw.bothell.css.dsl.MASS.Parallel_IO;

/**
 * Created by Michael on 4/19/17.
 */
@SuppressWarnings("serial")
public class InvalidNumberOfNodesException extends Exception {
    public InvalidNumberOfNodesException(String message) {
        super(message);
    }
}
