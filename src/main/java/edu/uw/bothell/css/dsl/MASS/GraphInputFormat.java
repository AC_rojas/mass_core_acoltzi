package edu.uw.bothell.css.dsl.MASS;

public enum GraphInputFormat {
    CSV,
    FILEGEN,
    MATSIM,
    HIPPIE,
    ;
}
