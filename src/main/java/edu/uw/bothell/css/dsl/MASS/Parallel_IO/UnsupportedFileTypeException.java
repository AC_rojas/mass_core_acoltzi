package edu.uw.bothell.css.dsl.MASS.Parallel_IO;

/**
 * Created by Michael on 4/17/17.
 */
@SuppressWarnings("serial")
public class UnsupportedFileTypeException extends Exception {
    public UnsupportedFileTypeException(String message) {
        super(message);
    }
}
