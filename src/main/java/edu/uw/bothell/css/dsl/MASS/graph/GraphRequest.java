package edu.uw.bothell.css.dsl.MASS.graph;

public interface GraphRequest {
    Object process();
}
